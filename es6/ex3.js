// EX3 - Arrow functions.

var inputs = process.argv.slice(2);

var result = inputs.map(elem => elem.charAt(0))
             .reduce((prevValue, elem) => prevValue + elem);

console.log(`[${inputs}] becomes "${result}"`);
