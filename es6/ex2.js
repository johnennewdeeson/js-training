// EX2 - String templates

var name = process.argv[2];
var nameLower = name.toLowerCase();

var result = `Hello, ${name}!
Your name lowercased is "${nameLower}".`;

console.log(result);
