// EX 7 - REST arrays.

module.exports = function average(...args) {
	let total = args.reduce((prev, elem) => prev + elem, 0);
	return total / args.length;
};
