// EX 10 - tempate tagging.

console.log(html`<b>${process.argv[2]} says</b>: "${process.argv[3]}"`);

function sanitize(x) {
      switch (x) {
        case "'": return '&apos;';
        case "\"": return '&quot;';
        case "<": return '&lt;';
        case ">": return '&gt;';
        case "&": return '&amp;';
      }
}

function html(elements, ...args) {
  // Sanitize using map.
  let variables = args.map(elem => elem.replace(/['"<>&]/g, x => sanitize(x)));

  // Trying to be clever here, join together using a reduce, except the two need to be equal length.
  variables.push("");
  return elements.reduce((sum, elem, index) => sum + elem + variables[index], "");
}
