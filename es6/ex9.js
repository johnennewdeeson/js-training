// EX 9 - default arguments 2, make important

module.exports = (text, y = text.length) => text + "!".repeat(y);
