// EX 8 - default arguments

module.exports = (x = 0, y = 1) => (y + x) / 2;
