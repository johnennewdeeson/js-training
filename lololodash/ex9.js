// EX 9 - Start templating

    // include the Lo-Dash library
    var _ = require("lodash");

    var worker = function(data) {
      return _.template('Hello <%= name %> (logins: <%= login.length %>)')(data);
    };

    // export the worker function as a nodejs module
    module.exports = worker;
