// EX 8 - Analyze

    // include the Lo-Dash library
    var _ = require("lodash");

    var worker = function(freelancers) {
        var average = _.reduce(freelancers, function(acc, elem, index) {
          return acc + elem.income;
        }, 0) / freelancers.length;

        var underperform = _.chain(freelancers)
          .filter(function(elem) {
            return elem.income <= average;
          })
          .sortBy('income')
          .value();


        var overperform = _.chain(freelancers)
          .filter(function(elem) {
            return elem.income > average;
          })
          .sortBy('income')
          .value();

        return {
          average: average,
          underperform: underperform,
          overperform: overperform
        };
    };

    // export the worker function as a nodejs module
    module.exports = worker;
