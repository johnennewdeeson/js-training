// EX 6 - Count the Comments

    // include the Lo-Dash library
    var _ = require("lodash");

    var worker = function(userComments) {
       return _.chain(userComments)
         .groupBy('username')
         .map(function(elem, index, collection) {
            return {
              username: index,
              comment_count: _.size(elem)
            };
         })
         .sortBy('comment_count')
         .reverse()
         .value();
    };

    // export the worker function as a nodejs module
    module.exports = worker;
