// Ex 7 - Give Me an Overview

    // include the Lo-Dash library
    var _ = require("lodash");

    var worker = function(orders) {
      return _.chain(orders)
        .reduce(function (acc, value, index, collection) {
           let articleId = value.article;
           acc[articleId] = (acc[articleId] == undefined) ? value.quantity : acc[articleId] + value.quantity;
           return acc;
        }, {})
        .map(function (elem, index) {
          return { article: parseInt(index), total_orders: elem };
        })
        .sortBy("total_order")
        .reverse()
        .value();
    };

    // export the worker function as a nodejs module
    module.exports = worker;
