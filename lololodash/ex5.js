// EX 5 - Chain Mail

    // include the Lo-Dash library
    var _ = require("lodash");

    var worker = function(list) {
      return _.chain(list)
        .map(function(o) {
          oChained = o + "chained";
          return oChained.toUpperCase();
        })
        .sortBy()
        .value();
    };

    // export the worker function as a nodejs module
    module.exports = worker;
