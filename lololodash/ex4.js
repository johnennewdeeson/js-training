// EX 4 - Everyone is Min

    // include the Lo-Dash library
    var _ = require("lodash");

    var worker = function(townTemps) {
        let sorted = {
          hot: [],
          warm: []
        };

        _.forEach(townTemps, function(temps, index) {
          if (_.every(temps, function(elem) { return elem >= 19; })) {
            // Its hot.
            sorted.hot.push(index);
          }
          else if (_.some(temps, function(elem) { return elem >= 19; })) {
            // Its warm.
            sorted.warm.push(index);
          }
        });

        return sorted;
    };

    // export the worker function as a nodejs module
    module.exports = worker;
