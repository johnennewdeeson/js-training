// EX9 - Combining observables 2

    module.exports = (Bacon, sector1Count, sector2Count, sector3Count, sector4Count) => {
      const sector5Count = sector1Count.map(v => 0);
      const deploymentReport = Bacon.combineTemplate({
        sector1: sector1Count,
        sector2: sector2Count,
        sector3: sector3Count,
        sector4: sector4Count,
        sector5: sector5Count});
      return deploymentReport;
    };
