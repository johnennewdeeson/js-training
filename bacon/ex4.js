// EX4 - Event Streams.

// Export method as a module.
module.exports = (Bacon) => {

  const bus = new Bacon.Bus();

  setTimeout(function() {
    bus.push('Bacon');
    bus.push('is');
    bus.push('delicious');
    bus.end();
  }, 1000);

	/**
	 * Your code here
	 **/
	return bus;
};
