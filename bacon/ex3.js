// Ex 3 - Subscription based

// expose the stream generator as a module method
module.exports = (Bacon, stream, action, actionOnValue) => {

  stream
    .doAction(action)
    .log('Value:')
    .onValue(actionOnValue);

	/**
	 * Your code
	 */
	return stream;
};
