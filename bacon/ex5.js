// EX 5 - Properties.

// Export method as a module.
module.exports = (Bacon) => {

  const bus = new Bacon.Bus();

  setTimeout(function() {
    bus.push(11);
    bus.push(12);
    bus.push(13);
    bus.end();
  }, 1000);

  const property = bus.toProperty(10);

  /**
   * Your code here
   **/
  return property;
};
