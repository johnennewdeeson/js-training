// EX 12 - Selective data by timing.

    module.exports = (Bacon, riverQuality, untilSwitchTurnedOff, sampleTime) => {

      const createdEventStream = riverQuality.debounceImmediate(sampleTime).takeWhile(untilSwitchTurnedOff);

      return createdEventStream;
    };
