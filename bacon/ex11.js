// EX 11 - Sampled

    module.exports = (Bacon, nidelva, leirelva, buttonClicked) => {
      const n1 = nidelva.sampledBy(buttonClicked);
      const l1 = leirelva.sampledBy(buttonClicked);
      return n1.combine(l1, (n, l) => n + l).toEventStream();
    };
