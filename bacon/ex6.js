// EX 6 - Map and Filter

    module.exports = (Bacon, enteringShips, destroyerPosition) => {

			// A stream which fires on every ship a 1 when its zrrk and 0 when its anything else.
			const zrrkShips = enteringShips
				.map(c => c.type == 'zrrk' ? 1 : 0);

			// A property that fires as the position changes with a label for the threat level based on distance.
			const threatLevel = destroyerPosition.map(function(x) {
				if (x > 5) return 'low';
				if (x > 2) return 'medium';
				if (x >= 1) return 'high';
				return 'extreme';
			});

			// A property that only starts firing when we get to extreme threats.
			const extremeThreat = threatLevel.filter(c => c === 'extreme');

			// Now lets take ships stream, but filter it to only start firing when the extreme threat stream is firing.
			const extremeThreatShips = zrrkShips.filter(extremeThreat);

      return {
        ships: zrrkShips,           // Your ship counter goes here
        threat: threatLevel,          // Your threat level goes here
        postArrivalShips: extremeThreatShips // Your ship counter post arrival goes here
      };
    };
