// EX 2 - Wrapping Values as Reactive Datatypes.

// Expose the stream generator as a module method.
module.exports = (Bacon, promise, eventTarget, callback) => {
	return {
		promise: Bacon.fromPromise(promise),
		eventTarget: Bacon.fromEvent(eventTarget, 'data'),
		callback: Bacon.fromCallback(callback, 'foo', 'bar'),
		array: Bacon.fromArray([1, 2, 3, 4])
  }
};
