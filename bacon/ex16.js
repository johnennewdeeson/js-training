// EX 16 - Error handling

    module.exports = (Bacon, asyncTask) => {
      return asyncTask.map(false).mapError(true).toProperty(false);
    };
