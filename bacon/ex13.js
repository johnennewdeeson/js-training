// EX 13 - Flat maps.

    module.exports = (Bacon, riverFlowInCubicFeet, litresInCubicFeet) => {
      return riverFlowInCubicFeet.flatMap(
					(tuple) => { 
						const waterLevel = tuple[0];
            const numberOfSamples = tuple[1];	
            const waterLevelLitres = Math.round(waterLevel * litresInCubicFeet);

            if (waterLevelLitres < 200000) {
              return Bacon.never();
            }

						return Bacon.interval(10, waterLevelLitres).take(numberOfSamples);
					}
      );
    };

