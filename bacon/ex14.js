// Ex 14 - Field and form validation

    module.exports = (Bacon, fieldA, validationA, fieldB, validationB, fieldC, validationC) => {
      const fieldAValid = fieldA.flatMap(validationA).toProperty(false);
      const fieldBValid = fieldB.flatMap(value => value ? validationB(value) : true).toProperty(true);
      const fieldCValid = fieldC.flatMap(validationC).toProperty(false);

      return {
        fieldAValid: fieldAValid,
        fieldBValid: fieldBValid,
        fieldCValid: fieldCValid,
        formValid: fieldAValid.and(fieldBValid).and(fieldCValid)
      };
    };

