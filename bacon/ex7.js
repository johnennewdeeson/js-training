// EX7 - fold and scan.

    module.exports = (Bacon, enteringShips, destroyerPosition) => {

      // Keep a tally of all zrrk ships entering the solar system.
      const shipTally = enteringShips.scan(0, function(acc, ship) {
        return ship.type === 'zrrk' ? ++acc : acc;
      });

      // Emit an event when number of ships reaches 5 after threat is less than 1.

      // 1. Determine when threat is less than 1.
      const threat = destroyerPosition.filter(x => x < 1);

      // 2. Fold shipTally only when threat is less than 1. Because its a fold we need to stop emitting after 5 values were emitted.
      const threatReport = enteringShips
        .filter(threat)
        .take(5)
        .fold({}, function(acc, ship) {
          acc[ship.type] = acc[ship.type] ? ++acc[ship.type] : 1;
          return acc;
        });

      /**
       * Your code here
       */
      return {
        shipTally: shipTally, //The running tally of ships goes here
        threatReport: threatReport, //The report of ships immediately following the Destroyer goes here
      };
    };
