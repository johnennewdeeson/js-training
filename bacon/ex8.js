// EX8 - Combining observables.

    module.exports = (Bacon, messages, keys, decoderFunction) => {
      const streamOfDecodedMessages = messages.zip(keys).map(v => decoderFunction(v));
      return streamOfDecodedMessages;
    };
