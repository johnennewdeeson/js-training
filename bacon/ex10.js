// EX10 - And or Or or Maybe not

    module.exports = (Bacon, riverFlow, inCriticalMode, isOnBreak, isSingleGate, systemActive, riverFlowLimit) => {
      const shouldReport = isOnBreak.not().and(inCriticalMode.or(systemActive.and(isSingleGate)))
      const riverFlowCritical = riverFlow.map(v => v > riverFlowLimit).toProperty();
      return riverFlowCritical.and(shouldReport);
    };
