// EX 1 - introduction.

module.exports = (Bacon) => {
  var args = [1, 2, 3];
  return Bacon.sequentially(100, args);
};
