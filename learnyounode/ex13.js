// Ex13 - HTTP JSON API SERVER

var http = require('http');
var url = require('url');
var strftime = require('strftime');

var port = process.argv[2];

function parsetime(time) {
  return {
    hour: time.getHours(),
    minute: time.getMinutes(),
    second: time.getSeconds()
  }
}

function unixtime(time) {
  return {
    unixtime: time.getTime()
  }
}

var server = http.createServer(function (req, res) {
  var reqUrl = url.parse(req.url, true);
  var iso = reqUrl.query.iso;

  var reqDate = new Date(iso);
  var body;

  if (reqUrl.pathname === '/api/parsetime') {
    body = parsetime(reqDate);
  }
  else if (reqUrl.pathname === '/api/unixtime') {
    body = unixtime(reqDate);
  }

  if (body) {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(body, ['hour', 'minute', 'second', 'unixtime'], 1) + "\n");
  }
  else {
    res.writeHead(404);
    res.end();
  }
});

server.listen(port)
