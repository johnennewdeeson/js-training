// Ex10 - time server
// I used strftime module for the win - https://github.com/samsonjs/strftime

var net = require('net');
var strftime = require('strftime');

var port = process.argv[2];

var server = net.createServer(function (socket) {
  var data = strftime('%Y-%m-%d %H:%M') + "\n";
	socket.write(data);
  socket.end();
})

server.listen(port)
