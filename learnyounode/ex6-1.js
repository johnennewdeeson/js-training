// Ex5: Filtered LS

var fs = require('fs');
var path = require('path');

module.exports = function(dirname, extension, node) {
  var files = [];
  extension = '.' + extension;

  fs.readdir(dirname, function (err, list) {
    if (err) return node(err);

    for (var i = 0; i < list.length; i++) {
      var listFile = list[i];
      var listFileExt = path.extname(listFile);

      if ( listFileExt == extension ) {
        files.push(listFile);
      }
    }

    node(null, files);
  });
};
