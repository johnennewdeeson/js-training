// Exercise 4: Async I/O

var fs = require('fs')

fs.readFile(process.argv[2], function callback (err, data) {
  var contents = data.toString();
  var lines = contents.split('\n');
  console.log(lines.length - 1);
})

