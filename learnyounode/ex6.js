// Ex6: Make a module

var mymodule = require('./ex6-1');

var dirname = process.argv[2];
var extension = process.argv[3];

mymodule(dirname, extension, function(err, data) {
  for (var i = 0; i < data.length; i++) {
    console.log(data[i]);
  }
});
