// Ex6: Filtered LS

var fs = require('fs');
var path = require('path');

var dirname = process.argv[2];
var extension = process.argv[3];

fs.readdir(dirname, function callback (err, list) {
  for (var i = 0; i < list.length; i++) {
    var listFile = list[i];
    var listFileExt = path.extname(listFile);

    if ( listFileExt == '.' + extension) {
      console.log(listFile);
    }
  }
});
