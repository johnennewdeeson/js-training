// Exercise 3: My First I/O

var fs = require('fs')

var buffer = fs.readFileSync(process.argv[2])
var contents = buffer.toString()
var lines = contents.split('\n')

console.log(lines.length - 1);
