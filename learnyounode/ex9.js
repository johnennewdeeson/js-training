// Ex9 - juggling async

var http = require('http');

var urls = [ process.argv[2], process.argv[3], process.argv[4] ];
var urlResponseStrs = ["", "", ""];

function urlResolver(response, delta) {
  var responseString = '';
  response.setEncoding("utf8");

  response.on("data", function (data) {
    responseString = responseString + data;
  })

  response.on("end", function () {
    urlResponseStrs[delta] = responseString;

    // Decide if every URL has returned.
    for (var i = 0; i < urlResponseStrs.length; i++) {
      if (urlResponseStrs[i].length == 0) return;
    }

    // Print out all the returned URLs.
    for (var i = 0; i < urlResponseStrs.length; i++) {
      console.log(urlResponseStrs[i]);
    }
  });
}

// Why can't I wrap this in a for loop?!

http.get(urls[0], function (response) { urlResolver(response, 0) });
http.get(urls[1], function (response) { urlResolver(response, 1) });
http.get(urls[2], function (response) { urlResolver(response, 2) });
