// Ex8 - http collect

var http = require('http');

var url = process.argv[2];

http.get(url, function callback(response) {
  var responseString = '';
  response.setEncoding("utf8");

  response.on("data", function (data) {
    responseString = responseString + data;
  })

  response.on("end", function () {
    console.log(responseString.length);
    console.log(responseString);
  });
});
