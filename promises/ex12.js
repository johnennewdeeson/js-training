// EX 12 - Get JSON.

var http = require("q-io/http");

http.read('http://localhost:1337').then(function(value) {
  var json = JSON.parse(value);
  console.log(json);
})
.catch(console.error)
.done();
