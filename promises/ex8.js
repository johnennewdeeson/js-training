// EX8 - Values and promises

function attachTitle(name) {
  return 'DR. ' + name;
}

var fulfilledPromise = new Promise(function(fulfil, reject) {
  fulfil('MANHATTAN');
});

fulfilledPromise.then(attachTitle).then(console.log);
