// EX 13 - Do some work

var http = require('q-io/http');

http.read('http://localhost:7000').then(function(userId) {
  return http.read('http://localhost:7001/' + userId);
})
.then(function (value) {
  console.log(JSON.parse(value));
})
.catch(console.error)
.done();

