// EX 6 - Shortcuts

var promiseFulfilled = Promise.resolve('fulfilled');
promiseFulfilled.then(console.log);

var promiseRejected = Promise.reject(new Error('rejected'));
promiseRejected.catch(function(error) {
  console.log(error.message);
});

console.log('Main');
