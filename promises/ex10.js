// EX 10 - An important rule

function alwaysThrows() {
  throw new Error("OH NOES");
}

function iterate(num) {
  return num + 1;
}

var newPromise = new Promise(function(fulfill, reject) {
  fulfill(1);
});

newPromise
  .then(function(value) {
    for (var i = 1; i <= 10; i++) {
      console.log(i);
      if (i == 5) alwaysThrows();
    }
  })
.catch(console.log);
