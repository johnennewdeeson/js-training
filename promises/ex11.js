// EX 11 - Multiple promises.

function all(promiseOne, promiseTwo) {

  var returnPromise = new Promise(function(fulfill, reject) {

    var num = 0;
    var values = [];

    var promiseResolver = function(value) {
      num++;
      values.push(value);
      if (num == 2) {
        fulfill(values);
      }
    }

    promiseOne.then(promiseResolver);
    promiseTwo.then(promiseResolver);
  });

  return returnPromise;

}

all(getPromise1(), getPromise2()).then(console.log);
