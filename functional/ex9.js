// EX 9 - Partial Application without Bind

var slice = Array.prototype.slice

function logger(namespace) {
		return function(...args) {
			args.unshift(namespace);
			console.log.apply(console, args);
		};
}

module.exports = logger
