// EX 12 - Function Spies

function Spy(target, method) {
  var oldFunc = target[method];

	var result = {
		count: 0
	};

	target[method] = function(...args) {
		result.count++;
		return oldFunc.apply(target, args);
  };

  return result;
}

module.exports = Spy
