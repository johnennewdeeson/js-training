// EX 16 - Recurrsion

    function getDependencies(tree, a = true) {
      // No dependencies? Just quit out with an empty list.
			if (!tree || !tree.dependencies) return [];

			return Object.keys(tree.dependencies)
        // First produce a list of the dependencies.
        .reduce(function(acc, elem, index, arr) {
          var dependencyTree = tree.dependencies[elem];
          var dependency = elem + '@' + dependencyTree.version;
          return acc.concat([ dependency ], getDependencies(dependencyTree, false));
        }, [])
        // Then sort the list alphabetically.
        .sort()
        // And finally remove the duplicates.
        .filter(function(elem, index, arr) {
          return  arr.indexOf(elem) === index;
        });
    }

module.exports = getDependencies
