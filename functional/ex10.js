// EX 10 - partical function with bind

module.exports = function(namespace) {
  return console.log.bind(console, namespace);
}
