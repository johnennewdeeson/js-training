// EX 14 - Trampoline

function repeat(operation, num) {
	return operation();
}

function trampoline(fn) {
	while (num > 0) {
	  var func = repeat(operation, num);
		setTimeout(func);
		num--;
	}
}

module.exports = function(operation, num) {
	// You probably want to call your trampoline here!
	return repeat(operation, num);
}
