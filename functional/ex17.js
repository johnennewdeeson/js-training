// Ex 17 - Currying

    function innerFunction(fn, n, ...args) {
      if (n == 0) {
        return fn(...args);
      }

      return function(arg) {
        return innerFunction(fn, n-1, ...args, arg);
      }

    };

    function curryN(fn, n) {
      n = n || fn.length;
      return innerFunction(fn, n);
    }

    module.exports = curryN
