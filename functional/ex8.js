// EX8 - Basic: Call

function duckCount() {
  return Array.from(arguments).reduce(function(acc, elem, index) {
    return Object.prototype.hasOwnProperty.call(elem, 'quack') ? acc + 1 : acc;
  }, 0);
}

module.exports = duckCount
