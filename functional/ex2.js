// EX2 - functions as variables.

function repeat(operation, num) {
  operation();
  if (num > 0) {
    repeat(operation, --num);
  }
}

// Do not remove the line below
module.exports = repeat
