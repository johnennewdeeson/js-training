// EX 11 - Implement map with reduce

module.exports = function arrayMap(arr, fn) {
  return arr.reduce(function(acc, elem, index) {
    acc.push(fn(elem));
    return acc;
  }, []);
}
