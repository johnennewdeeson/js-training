// EX6 - Reduce
function countWords(inputWords) {
  return inputWords.reduce(function(acc, elem, index) {
    acc[elem] = (acc[elem] == undefined) ? 1 : ++acc[elem];
    return acc;
  }, {});
}

module.exports = countWords
