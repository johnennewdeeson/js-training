// EX7 - recursion

function reduce(arr, fn, initial) {
  return (function reduceOne(index, value) {
    if (index >= arr.length) return value;
    var newValue = fn(value, arr[index], index, arr);
    return reduceOne(++index, newValue);
  })(0, initial);
};

module.exports = reduce;
