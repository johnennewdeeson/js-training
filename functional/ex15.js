// EX 15 - Async loops.

function loadUsers(userIds, load, done) {
  var users = [];
  var processed = 0;

  userIds.foreach(function(id, index) {
    load(id, function(user) {
      if (user) {
        users.push(user);
      }

      if (++processed == userIds.length) {
        done(users);
      }
    });
  });
}

module.exports = loadUsers
